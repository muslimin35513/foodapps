import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import LoginScreen from './src/login/login.js'
import Home from './src/home/home.js'
export default class App extends React.Component {
  render() {
    return (
      <Home />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
