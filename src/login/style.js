const React = require("react-native");

const { StyleSheet } = React;

export default {

containerView: {
  flex: 1,
},
loginScreenContainer: {
  flex: 1,
  backgroundColor:'#0f3460',
},
logoText: {
  fontSize: 40,
  fontWeight: "800",
  marginTop: 150,
  marginBottom: 30,
  textAlign: 'center',
  color:'#ffff',
},
loginFormView: {
  flex: 1,
  marginTop: 50,
},
loginFormTextInput: {
  height: 43,
  fontSize: 14,
  borderRadius: 5,
  borderWidth: 1,
  borderColor: '#eaeaea',
  backgroundColor: '#fafafa',
  paddingLeft: 10,
  marginTop: 10,
  marginBottom: 5,
  marginHorizontal:50,

},
loginButton: {
  backgroundColor: '#ffa41b',
  borderRadius: 5,
  height: 45,
  marginHorizontal:50,
  marginTop: 10,
},
};
